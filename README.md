# README #

This description provides a brief excursion through the different code files used in the development of the concept and the first prototype of the stimulation system based on the activation of stimulation based on the incoming motion signal.

### Folders included ###

* Filters (folder with filters reviewed)
* Webserver_on_esp (folder with files with initial experiments with esp 8266 and MQTT and webserwers)
* tens_control_prototype (folder with code running in esp32 to controll switching via BT)
* 

### Separate files ###
* bt test.py (code to send bloototh commands to esp32 after reading "outdemo.csv" file)
* movement_file.xlsx (original movement file)
* outdemo.csv ( analysed file by neural network with added stimulation values)
* recognition_by_FF_net.m (main code of matlab NN file)

### How to build and upload esp32, esp8266 and filter files ###

The Arduino IDE was used to upload the program files to the controllers, respectively the boards "doit esp32 devkit" b "spurkfun esp8266 think dev". The files itself were modified and developed in Visual Studio Code.


### Who do I talk to? ###

* Repo owner and admin is Nikolai Grigorjev
* In case of reusing those files and need of support or finding some not uploaded file contact the repo owner