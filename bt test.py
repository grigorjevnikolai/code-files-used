import serial
import time
from csv import DictReader
import pandas as pd
import matplotlib.pyplot as plt
active = '1'
prev_mode = 0
with serial.Serial() as ser:
    ser.baudrate = 9600
    ser.port = 'COM5'
    ser.open()
    with open('outdemo.csv', 'r') as read_obj:
        measured_data = DictReader(read_obj)
        print("lets start stimulation")
        for row in measured_data:
            time.sleep(0.004) # Delay for 0.004 sec (measurement frq 250 -> 1/250).
            #Python is nor real time, even on linux with kernel real time.
            #Python is fantastic but don’t forget “it’s an high level language”, 
            # for a good manage with fast timer, you need a low level language like C
            if row['Stimulation'] == active :
                if prev_mode == 0:
                    ser.write(b'on\r\n')
                    input_data=ser.readline()#This reads the incoming data
                    print(input_data.decode())
                    prev_mode = 1
                else:
                    prev_mode = 1
            else:
                if prev_mode == 1:
                    ser.write(b'off\r\n')
                    input_data=ser.readline()#This reads the incoming data
                    print(input_data.decode()) 
                    prev_mode = 0
                else:
                    prev_mode = 0

    ser.close()
print("stimulation stopped, draving nice chart")
df = pd.read_csv('outdemo.csv')
fig, ax1 = plt.subplots()

ax2 = ax1.twinx()
ax2.set_ylim([-1, 2])
ax1.plot(df['Shimmer_CB64_Timestamp_Unix_CAL'], df['Shimmer_CB64_Gyro_X_CAL'], 'b', label="gyroscope x")
ax2.plot(df['Shimmer_CB64_Timestamp_Unix_CAL'], df['Stimulation'], 'r', label="stimulation")
ax1.set_xlabel('Number of sample (datapoints)')
ax1.set_ylabel('Gyroscope X axis (deg/s)', color='b')
ax2.set_ylabel('Stimulation', color='r')

#plt.legend()
plt.show()
print("File reading ended")