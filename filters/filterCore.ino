// as refenrence used https://github.com/AlexGyver/GyverMatrixWiFi/ files and examples
int count = 0;            // counter
bool flag = 0;
float cleanSignal = 0.0;  // original clean signal
float noiseSignal = 0.0;  // signal + noise
float filtSignal = 0.0;   // filtered signal

void setup() {
  Serial.begin(9600);
  delay(500);
  Serial.flush();
  Serial.println("clean, noise, filter");
  delay(100);
}

void loop() {
  process();
  measure();
  Serial.print(cleanSignal);
  Serial.print(',');
  Serial.print(noiseSignal);
  Serial.print(',');
  Serial.println(filtSignal);
  count++;
}

// measuring with given period
void measure() {
  static uint32_t tmr;
  if (millis() - tmr >= 5) {
    tmr = millis();
    // filter selection
    //filtSignal = midArifm();
    //filtSignal = midArifm2(getSignal());
    //filtSignal = runMiddleArifmBad(getSignal());
    //filtSignal = runMiddleArifm(getSignal());
    //filtSignal = runMiddleArifmOptim(getSignal());
    //filtSignal = expRunningAverage(getSignal());
    //filtSignal = expRunningAverageAdaptive(getSignal());
    //filtSignal = median(getSignal());
    //filtSignal = expRunningAverage(filtSignal); // + to median
    //filtSignal = simpleKalman(getSignal());
    filtSignal = ABfilter(getSignal());
  }
}

// signal generation
void process() {
  // sinus signal
  cleanSignal = 4.0 * sin(0.5 * radians(count));

  // square
  //if (count % 100 == 0) flag = !flag; // every 100 ticks invert the signal
  //cleanSignal = flag * 3;  // square with amplitude 3
}

float getSignal() {
  noiseSignal = cleanSignal;

  // constatnt noise
  noiseSignal += (random(-8, 9) / 10.0);

  // random noise
  if (!random(30)) noiseSignal += random(-2, 1);

  return noiseSignal;
}
