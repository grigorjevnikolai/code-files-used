// as refenrence used https://github.com/AlexGyver/GyverMatrixWiFi/ files and examples
const int NUM_READ = 30;  // the number of averaging for arithma means. filters

// ordinary arithmetic mean
float midArifm() {
  float sum = 0;                      // local var sum
  for (int i = 0; i < NUM_READ; i++)  // according to the number of averaging
    sum += getSignal();               // sum the values from any sensor into the variable sum
  return (sum / NUM_READ);
}

// stretched arithmetic mean
float midArifm2(float newVal) {
  static byte counter = 0;
  static float prevResult = 0;
  static float sum = 0;
  sum += newVal;
  counter++;
  if (counter == NUM_READ) {
    prevResult = sum / NUM_READ;
    sum = 0;
    counter = 0;
  }
  return prevResult;
}

// running arithmetic mean not optimal
float runMiddleArifmBad(float newVal) {  // takes on a new measuring
  static float valArray[NUM_READ];    // array

  for (int i = 0; i < NUM_READ - 1; i++)
    valArray[i] = valArray[i + 1];

  valArray[NUM_READ - 1] = newVal;    // write new to the outermost cell
  float average = 0;                  // average

  for (int i = 0; i < NUM_READ; i++)
    average += valArray[i];           // sum

  return (float)average / NUM_READ;   // return
}

// running arithmetic mean
float runMiddleArifm(float newVal) {  // takes on a new measuring
  static byte idx = 0;                // index
  static float valArray[NUM_READ];    // array
  valArray[idx] = newVal;             // we write each time to a new cell
  if (++idx >= NUM_READ) idx = 0;     // overwriting the oldest value
  float average = 0;                  // 0 average
  for (int i = 0; i < NUM_READ; i++) {
    average += valArray[i];           // sum
  }
  return (float)average / NUM_READ;   // return
}

// optimal running arithmetic mean
float runMiddleArifmOptim(float newVal) {
  static int t = 0;
  static int vals[NUM_READ];
  static int average = 0;
  if (++t >= NUM_READ) t = 0; // change t
  average -= vals[t];         // subtract the old
  average += newVal;          // add new
  vals[t] = newVal;           //store in array
  return ((float)average / NUM_READ);
}

// running average, even more optimal version of the previous filter
float expRunningAverage(float newVal) {
  static float filVal = 0;
  filVal += (newVal - filVal) * 0.2;
  return filVal;
}

// running average with adaptive coefficient
float expRunningAverageAdaptive(float newVal) {
  static float filVal = 0;
  float k;
  // the sharpness of the filter depends on the absolute value of the difference
  if (abs(newVal - filVal) > 1.5) k = 0.9;
  else k = 0.03;

  filVal += (newVal - filVal) * k;
  return filVal;
}

//median by 3 values with its own buffer
float median(float newVal) {
  static float buf[3];
  static byte count = 0;
  buf[count] = newVal;
  if (++count >= 3) count = 0;

  float a = buf[0];
  float b = buf[1];
  float c = buf[2];

  float middle;
  if ((a <= b) && (a <= c)) {
    middle = (b <= c) ? b : c;
  } else {
    if ((b <= a) && (b <= c)) {
      middle = (a <= c) ? a : c;
    } else {
      middle = (a <= b) ? a : b;
    }
  }
  return middle;
}

// simplified kalman
float _err_measure = 0.8;  // approx noise level
float _q = 0.1;   // value change spee 0.001-1, to be self changed

float simpleKalman(float newVal) {
  float _kalman_gain, _current_estimate;
  static float _err_estimate = _err_measure;
  static float _last_estimate;

  _kalman_gain = (float)_err_estimate / (_err_estimate + _err_measure);
  _current_estimate = _last_estimate + (float)_kalman_gain * (newVal - _last_estimate);
  _err_estimate =  (1.0 - _kalman_gain) * _err_estimate + fabs(_last_estimate - _current_estimate) * _q;
  _last_estimate = _current_estimate;

  return _current_estimate;
}

// sampling period (measurements), process variation, noise variation
float dt = 0.02;
float sigma_process = 3.0;
float sigma_noise = 0.7;

float ABfilter(float newVal) {
  static float xk_1, vk_1, a, b;
  static float xk, vk, rk;
  static float xm;

  float lambda = (float)sigma_process * dt * dt / sigma_noise;
  float r = (4 + lambda - (float)sqrt(8 * lambda + lambda * lambda)) / 4;
  a = (float)1 - r * r;
  b = (float)2 * (2 - a) - 4 * (float)sqrt(1 - a);

  xm = newVal;
  xk = xk_1 + ((float) vk_1 * dt );
  vk = vk_1;
  rk = xm - xk;
  xk += (float)a * rk;
  vk += (float)( b * rk ) / dt;
  xk_1 = xk;
  vk_1 = vk;
  return xk_1;
}
