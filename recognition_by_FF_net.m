%% initial data reading from measured file
Array=xlsread('movement_file.xlsx');
col1 = Array(17000:28000, 1);
col2 = Array(:, 11);
plot(col1, col2)
plot(col2)
set(gca,'FontSize',16,'FontWeight','bold')
xlabel('Number of sample (datapoints)','FontSize',17,'FontWeight','bold')
ylabel('Gyroscope X axis (deg/s)','FontSize',17,'FontWeight','bold')
%% manual one third of step
step1 = mat2gray(col2(17164:17273)); % normal step  17273 17493
step2 = mat2gray(col2(17494:17603)); % abnormal step 17603 17823
step3 = mat2gray(col2(17835:17944)); % normal step 17944 18164
step4 = mat2gray(col2(25788:25897)); % abnormal step 25897 26117
step5 = mat2gray(col2(16834:16943)); % normal step for test 16943 17163
step6 = mat2gray(col2(16494:16603)); % normal step for test 16603 16823
step7 = mat2gray(col2(21366:21475)); % abnormal step for test 21475 21695

%% manual one half of step
step1 = mat2gray(col2(17164:17328)); % normal step  17273 17493
step2 = mat2gray(col2(17494:17658)); % abnormal step 17603 17823
step3 = mat2gray(col2(17835:17999)); % normal step 17944 18164
step4 = mat2gray(col2(25788:25952)); % abnormal step 25897 26117
step5 = mat2gray(col2(16834:16998)); % normal step for test 16943 17163
step6 = mat2gray(col2(16494:16658)); % normal step for test 16603 16823
step7 = mat2gray(col2(21366:21530)); % abnormal step for test 21475 21695
%% manual full step
step1 = mat2gray(col2(17164:17493)); % normal step
step2 = mat2gray(col2(17494:17823)); % abnormal step
step3 = mat2gray(col2(17835:18164)); % normal step
step4 = mat2gray(col2(25788:26117)); % abnormal step
step5 = mat2gray(col2(16834:17163)); % normal step for test
step6 = mat2gray(col2(16494:16823)); % normal step for test
step7 = mat2gray(col2(21366:21695)); % abnormal step for test

%% training set preparation and step visualization
steps = [step1 step4]; % first training set
steps1 = [step3 step2]; % second training set
targets = eye(2);

% Array2=xlsread('hemiplegic_test1_foot_Shimmer_CA0E_Calibrated_SD.xlsx');
% col21 = Array2(:, 1);
% col22 = Array2(:, 11);
% figure; plot(number1)
figure; plot(step5)
figure; plot(step6)
 figure; plot(step7)

%% NN training part

net=newff(minmax(steps),[8 2],{'logsig','softmax'},'trainlm')
net.trainParam.epochs=5000;
net.trainParam.show=25;
net.trainParam.min_grad=1e-17;%17

P=[steps1, steps , steps+rand(110,2)*0.04,...
    steps1+rand(110,2)*0.04,...
    steps+rand(110,2)*0.6,...
    steps1+rand(110,2)*0.6,... %training network wit sets and sets with noise
    ];

T=[targets targets targets targets targets targets];

net=train(net,P,T)
%% Testing
%test_data=numbers; %+randn(35,10)*0.2;
%test=sim(net,test_data)
test=sim(net,step1)
test=sim(net,step2)
test=sim(net,step3)
test=sim(net,step4)
test=sim(net,step5)
test=sim(net,step6)
test=sim(net,step7)
%%
for i=1:10
    m=max(test(:,i));
    test_result(i)=find(test(:,i)==m);
end
test_result

%%
 noise_2=test_data(1:35,10); %used to visualize wrongly recognized elements
  vec2image(noise_2)
  
%% step count
% https://se.mathworks.com/help/matlabmobile_android/ug/counting-steps-by-capturing-acceleration-data.html
a=17000;
b=28000;
t = Array(a:b, 1);
x = Array(a:b, 2);
y = Array(a:b, 3);
z = Array(a:b, 4);
g=[x y z];
plot(t,x);
legend('X', 'Y', 'Z');
xlabel('Relative time (s)');
ylabel('Acceleration (m/s^2)');

mag = sqrt(sum(x.^2 + y.^2 + z.^2 , 2)); %  
plot(t,mag);
xlabel('Time (s)');
ylabel('Acceleration (m/s^2)');

%magNoG = mag - mean(mag);
magNoG = mag - mean(mag);
plot(t,magNoG);
%plot(t,x);
xlabel('Time (s)');
ylabel('Acceleration (m/s^2)');

minPeakHeight = std(magNoG);

[pks,locs] = findpeaks(magNoG,'MINPEAKHEIGHT',minPeakHeight);


numSteps = numel(pks)

hold on;
plot(t(locs), pks, 'r', 'Marker', 'v', 'LineStyle', 'none');
title('Counting Steps');
xlabel('Time (s)');
ylabel('Acceleration Magnitude, No Gravity (m/s^2)');
hold off;

%% zero crossing


windowSize = 30; 
f1 = (1/windowSize)*ones(1,windowSize);
f2 = 1;
X = filter(f1,f2,g);
accelScalar = sqrt(sum(X.^2, 2));
accelScalarNoGravity = accelScalar - 9.8;
plot(accelScalarNoGravity)
%plot(t,x);
aboveZero = accelScalarNoGravity > 0;
zeroCrossing = diff(aboveZero) == 1;
zeroCrossingIndex = find(zeroCrossing);
hold on
plot(zeroCrossingIndex, zeros(size(zeroCrossingIndex)), 'r', 'Marker', 'v', 'LineStyle', 'none')
hold off

%% adding stimulation bit
for i= size(zeroCrossingIndex)
test=sim(net,step(i))
if test == 1
    