#include "BluetoothSerial.h"
const int Pin1 =  13;
const int Pin2 =  27;
int blinkvalue =  0;
String message = "";
char incomingChar;

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;

void setup() {
  pinMode(Pin1, OUTPUT);
  pinMode(Pin2, OUTPUT);
  digitalWrite(Pin1, LOW);
  digitalWrite(Pin2, LOW);
  Serial.begin(115200);
  SerialBT.begin("TENS prototype"); 
  Serial.println("TENS prototype started, pair with bluetooth!");

}

void loop() {

  // Read received messages (LED control command)
  //if (Serial.available()) {
    //SerialBT.write(Serial.read());
  //}
  
  if (SerialBT.available()){
    
    char incomingChar = SerialBT.read();
    if (incomingChar != '\n'){
      message += String(incomingChar);
      //blinkvalue =  0;
    }
        
    else{
      message = "";
    }
    Serial.write(incomingChar);  
  }
  else if (blinkvalue ==  1 ){
      //Serial.write("blink again");
      message = "test";
      }
  // Check received message and control output accordingly
  if (message =="on"){
    digitalWrite(Pin1, HIGH);
    digitalWrite(Pin2, LOW);
    SerialBT.println("Stimulation turned ON");
    blinkvalue =  0;
    delay(1);
  }
  else if (message =="off"){
    digitalWrite(Pin1, LOW);
    digitalWrite(Pin2, HIGH);
    SerialBT.println("Stimulation turned OFF");
    blinkvalue =  0;
    delay(1);
  }
  else if (message =="test"){
    SerialBT.println("Stimulation in test mode");
    digitalWrite (Pin1, HIGH);
    digitalWrite (Pin2, LOW);
    delay(1000);
    digitalWrite (Pin1, LOW);
    digitalWrite (Pin2, HIGH);
    blinkvalue =  1;
    delay(980);
  }
  delay(20);
}
