#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "XXXXXX";
const char* password = "XXXXXXXX";
char* mqtt_server = "test.mosquitto.org"; //webserver version

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
#define RELAY 0 // relay connected to  GPIO0
char msg[MSG_BUFFER_SIZE];
int value = 0;
int x =0;
int y=1;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    
    if(y!=0){
      
    digitalWrite(RELAY, LOW); // Turn the LED on (Note that LOW is the voltage level
    Serial.print("low/ relay on");// but actually the LED is on; this is because
    client.publish("nikolai/sensor/relaystat", "relay ON");
    // it is active low on the ESP-01)
    }
  } 
  else if ((char)payload[0] == '2') {
   Serial.print(" controlled manually");
   client.publish("nikolai/sensor/relaycontrollmode", "controlled manually");
   y=0;
  }
  else if ((char)payload[0] == '3') {
   Serial.print("turned on, controlled by sensor");
   client.publish("nikolai/sensor/relaycontrollmode", "controlled by sensor");
   y=1;
  }
    else if ((char)payload[0] == '4') {
    
    if(y!=1){
     
    digitalWrite(RELAY, LOW); // Turn the LED on (Note that LOW is the voltage level
    Serial.print("low/ relay on");// but actually the LED is on; this is because
    client.publish("nikolai/sensor/relaystat", "relay ON");
    // it is active low on the ESP-01)
    }
  }
      else if ((char)payload[0] == '5') {
    
    if(y!=1){
      
    digitalWrite(RELAY, HIGH);
    Serial.print("high/relay off");// Turn the LED off by making the voltage HIGH
    client.publish("nikolai/sensor/relaystat", "relay OFF");
    // it is active low on the ESP-01)
    }
  }
  else {
   
    if(y!=0){
      
    digitalWrite(RELAY, HIGH);
    Serial.print("high/relay off");// Turn the LED off by making the voltage HIGH
    client.publish("nikolai/sensor/relaystat", "relay OFF");
        }
    }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      //client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("nikolai/sensor/relay");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//String macToStr(const uint8_t* mac)
//{
//  String result;
//  for (int i = 0; i < 6; ++i) {
//    result += String(mac[i], 16);
//    if (i < 5)
//      result += ':';
//  }
//  return result;
//}

void setup() {
  pinMode(RELAY, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();


}
